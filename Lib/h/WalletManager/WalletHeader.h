//
//  WalletHeader.h
//  WalletManager
//
//  Created by 王胜利 on 2018/6/8.
//  Copyright © 2018年 wsl. All rights reserved.
//

#ifndef Wallet_h
#define Wallet_h

#import "Constant.h"
#import "BlockChainManager.h"
#import "ReadPlistFile.h"

/**
 钱包类型

 - WalletTypeNormal: 普通钱包
 - WalletTypeSee: 观察钱包
 - WalletTypeCold: 冷钱包
 */
typedef NS_ENUM(NSUInteger, OpenWalletType) {
    OpenWalletTypeNormal           = 1,
    OpenWalletTypeSee              = 2,
    OpenWalletTypeCold             = 3,
};

/**
 添加新钱包方式

 - AddWalletTypeCreate: 创建
 - AddWalletTypeImport: 导入
 */
typedef NS_ENUM(NSUInteger, AddWalletType) {
    AddWalletTypeCreate            = 0,
    AddWalletTypeImport            = 1,
};



#define WalletUrl [ReadPlistFile readValueFromPlistName:@"OptionSetting.plist" valueKey:@"WalletUrl"]

#define WalletBundle @"Wallet.bundle"
#define WalletBundleImage(path) BundleImage(WalletBundle, path)


#endif /* Wallet_h */
