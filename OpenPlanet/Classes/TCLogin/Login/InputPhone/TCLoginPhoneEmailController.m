//
//  TCPhoneController.m
//  OSPTalkChain
//
//  Created by 王胜利 on 2018/3/10.
//  Copyright © 2018年 wsl. All rights reserved.
//  1.输入手机号或邮箱
//

#import "TCLoginPhoneEmailController.h"
#import "ThemeKit.h"
#import "TCLoginBaseView.h"
#import "TCInputPhoneTextField.h"
#import "TCLoginManager.h"
#import "TCInputPasswordController.h"
#import "TCChosePlanetController.h"
#import "UIViewController+TCHUD.h"
#import "SLAlertController.h"
#import "JFLanguageManager.h"
#import "Constant.h"
#import "Masonry.h"
#import "ThemeKit.h"
#import "CategoryHeader.h"
#import "TCIsAgreePrivatePolicyView.h"
#import "TCPrivateController.h"
#import "EFXHNavigationController.h"

@interface TCLoginPhoneEmailController ()

@property (nonatomic, strong) TCLoginBaseView *rootView;
@property (nonatomic, strong) UITextField *accountTextField;
/// 是否同意协议
@property (nonatomic, strong) TCIsAgreePrivatePolicyView *isAgreePrivateView;
/// 登录类型： 0：手机号 1:邮箱
@property (nonatomic, assign) NSInteger accountType;

@end

@implementation TCLoginPhoneEmailController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupForDismissKeyboard];
    
    [self rootView];
    
    self.rootView.nextButton.enabled = NO;
    self.rootView.nextButton.alpha= 0.4;
    
    // 如果有旧账户,自动显示用户名®®
    NSString *account = [EnvironmentVariable getWalletUserID];
    if (!StringIsEmpty(account)) {
        self.accountTextField.text = account;
        [self textChanged];
    }else{
        [self.accountTextField becomeFirstResponder];
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (self.accountTextField.text.length == 0) {
        [self.accountTextField becomeFirstResponder];
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}

// MARK: - 文本框内容变化时
- (void)textChanged{
    /// 判断手机号是否为空
    BOOL isRight = NO;
    //首先判断是不是手机号或者邮箱
    if ([self.accountTextField.text isValidateEmail] && self.isAgreePrivateView.isAgree){
        isRight = YES;
    }
        
    self.rootView.nextButton.enabled = isRight;
    self.rootView.nextButton.alpha= isRight?1:0.4;
}

#pragma mark - 查看协议
- (void)seeAgreementAction{
    TCPrivateController *privateVc = [TCPrivateController new];
    EFXHNavigationController *nvc = [[EFXHNavigationController alloc] initWithRootViewController:privateVc];
    nvc.navigationBar.theme_barStyle = [NSString theme_stringForKey:@"barStyle" from:@"navigationBar"];
    nvc.navigationBar.theme_tintColor = [UIColor theme_navigationBarColorForKey:@"tintColor"];
    nvc.navigationBar.theme_barTintColor = [UIColor theme_navigationBarColorForKey:@"barTintColor"];
    nvc.navigationBar.translucent = NO;
    [self presentViewController:nvc animated:YES completion:nil];
}

// MARK: - 下一步 Action
- (void)nextButtonAction:(UIButton *)sender{
    NSString *account = self.accountTextField.text;
    if (account.length == 0) {
        return;
    }
    
    [self.view endEditing:YES];
    
    
    WEAK(self)
    
    [self showTCHUDWithTitle:[JFLanguageManager stringWithKey:@"waitALittleLater" table:Table_Tools comment:@"请稍候..."]];
    
    if ([account isValidPhoneNum]){
        self.accountType = 0;
    }else if([account isValidateEmail]) {
        self.accountType = 1;
    }
    
    [TCLoginManager checkAccountIsExist:account success:^(id result) {
        [weakself hiddenTCHUD];
        if ([[result valueForKey:@"result"] isEqualToString:@"success"]) {
            if ([[result valueForKey:@"userStatus"] isEqualToString:@"1"]) {
                // 老用户密码登录
                TCInputPasswordController *vc = [TCInputPasswordController new];
                vc.accountType = weakself.accountType;
                vc.account = weakself.accountTextField.text;
                [weakself.navigationController pushViewController:vc animated:YES];
            }else if([[result valueForKey:@"userStatus"] isEqualToString:@"-1"]){
                // 新用户
                TCChosePlanetController *vc = [TCChosePlanetController new];
                vc.accountType = weakself.accountType;
                vc.account = weakself.accountTextField.text;
                [weakself.navigationController pushViewController:vc animated:YES];
            }
        }else{
            [weakself sl_showAlertWithTitle:JFLanguageManager.alert message:[JFLanguageManager stringWithKey:@"validationFailed" table:Table_Tools comment:@"验签失败"]];
        }
    } fail:^(NSString *errorDescription) {
        [weakself hiddenTCHUD];
        [weakself sl_showAlertWithTitle:JFLanguageManager.alert message:JFLanguageManager.requestFailed];
    }];
}

#pragma mark - lazyload
- (TCLoginBaseView *)rootView{
    if (!_rootView) {
        _rootView = [TCLoginBaseView new];
        /// 设置控件默认值
        _rootView.topBgImageView.theme_image = [UIImage theme_bundleImageNamed:@"login/login_phone_bg.png"];
        _rootView.tipImageView.theme_image = [UIImage theme_bundleImageNamed:@"login/login_phone.png"];
        _rootView.titleLabel.text = [NSString stringWithFormat:@"%@%@",[JFLanguageManager stringWithKey:@"openPlanetWelcomeTo" table:Table_OpenPlanet comment:@"欢迎使用"],APPName];
        _rootView.tipLabel.text = [JFLanguageManager stringWithKey:@"openPlanetLoginTip" table:Table_OpenPlanet comment:@"为方便给您提供更优质的服务，请输入您的手机或者邮箱"];
        [_rootView.nextButton setTitle:JFLanguageManager.next forState:UIControlStateNormal];
        [_rootView.nextButton addTarget:self action:@selector(nextButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_rootView];
        [_rootView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
        
        [self.rootView.customView addSubview:self.accountTextField];
        [self.accountTextField mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.top.right.equalTo(_rootView.customView);
            make.height.equalTo(@45);
        }];
        
        [self.rootView.customView addSubview:self.isAgreePrivateView];
        [self.isAgreePrivateView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.accountTextField);
            make.top.equalTo(self.accountTextField.mas_bottom).offset(5);
            make.bottom.equalTo(_rootView.customView);
        }];
        
        [self.view layoutIfNeeded];
    }
    return _rootView;
}

- (UITextField *)accountTextField{
    if (!_accountTextField) {
        _accountTextField = [UITextField new];
        _accountTextField.borderStyle = UITextBorderStyleNone;
        _accountTextField.keyboardType = UIKeyboardTypeEmailAddress;
        _accountTextField.font = FONT(15);
        _accountTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _accountTextField.theme_textColor = [UIColor theme_colorForKey:@"textFieldTextColor" from:@"login"];
        _accountTextField.theme_attributePlaceHolder = [NSAttributedString theme_attributeStringWithString:[JFLanguageManager stringWithKey:@"openPlanetPleaseEnterEmail" table:Table_OpenPlanet comment:@"请输入账号"] attributeColor:[UIColor theme_colorForKey:@"textFieldPlaceHolderColor" from:@"login"]];
        [_accountTextField addTarget:self action:@selector(textChanged) forControlEvents:UIControlEventEditingChanged];
        
        UIView *bottomLine = [UIView new];
        bottomLine.theme_backgroundColor = [UIColor theme_colorForKey:@"separatorColor" from:@"login"];
        [_accountTextField addSubview:bottomLine];
        [bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(_accountTextField).inset(-3);
            make.bottom.equalTo(_accountTextField);
            make.height.equalTo(@1);
        }];
    }
    return _accountTextField;
}

- (TCIsAgreePrivatePolicyView *)isAgreePrivateView{
    if (!_isAgreePrivateView) {
        _isAgreePrivateView = [TCIsAgreePrivatePolicyView new];
        _isAgreePrivateView.theme_backgroundColor = [UIColor theme_colorForKey:@"cardBackgroud" from:@"login"];

        WEAK(self)
        _isAgreePrivateView.agreementButtonActionBlock = ^{
            [weakself seeAgreementAction];
        };
        _isAgreePrivateView.agreeAgreementButtonActionBlock = ^{
            [weakself textChanged];
        };
    }
    return _isAgreePrivateView;
}



@end
