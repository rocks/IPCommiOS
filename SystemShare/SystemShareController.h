//
//  SystemShareController.h
//  Share
//
//  Created by 高建飞 on 2018/5/29.
//  Copyright © 2018年 wsl. All rights reserved.
//

#import <UIKit/UIKit.h>

#define AppGroupName    @"group.turbopay.openplanet"
#define SystemShareKey  @"shareKey"
//#define AppKey          @"OpenPlanetXJZC://"
#define AppKey          @"OpenPlanet://"

@interface SystemShareController : UIViewController

@end


#define BundleFile(bundle,path)  [[[NSBundle mainBundle] pathForResource:bundle ofType:nil] stringByAppendingString:path]
#define BundleImage(bundle,path) [UIImage imageWithContentsOfFile:BundleFile(bundle,path)]
