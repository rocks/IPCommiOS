<p align="center">
<img src="./assets/ipcomm_icon.png" width="150" />
</p>
<h1 align="center">IPCOM星际通讯</h1> 
<p align="center">
    <a href="http://rpa.openserver.cn">
        <img src="https://img.shields.io/badge/Licence-GPL3.0-green.svg?style=flat" />
    </a>    
     <a href="http://rpa.openserver.cn/download/RPAStudioSetup-v2.0.0.3.exe">
        <img src="https://img.shields.io/badge/download-80m-red.svg" />
    </a>
    </p>
<p align="center">    
    <b>如果对您有帮助，您可以点右上角 "Star" 支持一下 谢谢！</b>
</p>





### 项目介绍
​       星际通讯是基于区块链的价值共享互联网即时通讯应用平台，是一个去中心化的任何人都可以使用的通讯网络，是一款基于区块链的价值共享互联网即时通讯APP。星际通讯系统为人与设备、人与人、人与服务、服务与设备等提供高效、稳定、即时的网络通讯服务。通过区块链加密存储技术帮助您管理数字资产，支持即时消息通讯、离线消息；并支持文字、图片、语音、视频、表单及自定义消息类型。
​      星际通讯开源聊天项目，是星际通讯项目聊天部分的开源代码。完全由原生代码实现了单聊、群聊、公众号等聊天功能。聊天格式支持文字、表情、图片、视频、文件等常规内容，更拓展支持加密文本、加密图片等加密内容。

### 主要特点
#### 聊天
- 支持单聊、群聊、公众号等聊天功能；
- 支持文字、表情、图片、视频、文件等普通聊天格式；
- 支持加密文字、加密图片等加密内容；
- 支持语音实时输入发送（“光速短信”）；
- 数字货币红包；
- 支持公众号消息的推送服务；
#### 其他
- 空间（“引力场”）动态的发送、展示、评论互动功能；
- 数字货币的资产管理、转账功能；
- 数字货币的行情、K线图实时展示；
- 新闻资讯服务；
- 商城系统；
- 支持多语言
- 支持换肤

### 运行环境
  * MacOS 10.15
  * Xcode11
  * iOS 10.0 +

### 如何使用
#### submodule
##### 项目使用git的submodule来管理多个库工程依赖，如果clone下来的库工程没有代码，可以
##### 执行命令：`git clone --recursive <repo地址>` 来拉取
##### 拉取成功后
##### 执行命令：`git submodule update --remote` 更新所有子模块代码
##### 在子模块上提交等操作详见 [git submodule文档](https://git-scm.com/book/zh/v2/Git-%E5%B7%A5%E5%85%B7-%E5%AD%90%E6%A8%A1%E5%9D%97)

### 工程结构
```
OpenPlanet：主工程
ChatComps：聊天功能库
ThemeKit：主题库
ToolsLibrary：工具库
NewsComps：新闻资讯库
EShopComps：商城库
PayComps：支付库
StockMarket：行情库
HealthComps：健康库（开发中）
WebSystem：js组件基本库
WebComps：js组件扩展库
MobileManager：工程基础库
MobileView：工程基础View库
CloudDiskComps：云盘库
ethers：ETH钱包实现库
OSPBase：工程基础库
ACSBlueToothComps：第三方蓝牙硬件设备库
Charts：第三方图标库
UMComps：第三方友盟分享封装库
SDWebImage：第三方图片加载封装库
Masonry：第三方布局封装库
XMLParser：第三方XML解析封装库
DNManager：第三方FMDB封装库
AFNetWorking：第三方网络请求封装库
```


### 页面展示
<img src="./assets/left.png" width="300"  />
<img src="./assets/left_b.png" width="300" />
<img src="./assets/chat.png" width="300" />
<img src="./assets/chat_list.png" width="300" />
<img src="./assets/chat_more.png" width="300" />
<img src="./assets/chat_more2.png" width="300" />
<img src="./assets/address.png" width="300" />
<img src="./assets/news.png" width="300" />
<img src="./assets/zone.png" width="300" />


### 版权声明
  * 本软件使用 GPL3.0 协议，请严格遵照协议内容!
### 合作及联系
- QQ交流群: 976048137

- 联系邮箱：app@turbochain.ai
<img src="./assets/qq_ipcomm.jpg" width="300" />

### End


