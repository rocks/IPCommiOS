//
//  WalletDetailController.m
//  Watch Extension
//
//  Created by 王胜利 on 2019/3/11.
//  Copyright © 2019 pansoft. All rights reserved.
//

#import "WalletDetailController.h"
#import "Wallet+CoreDataClass.h"
#import "Wallet+CoreDataProperties.h"
#import "CoreDataManager.h"
#import "WKInterfaceImage+WKImageUrl.h"

@interface WalletDetailController ()

@property (nonatomic, strong) Wallet *info;
@property (weak, nonatomic) IBOutlet WKInterfaceImage *headImage;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *nameLabel;
@property (weak, nonatomic) IBOutlet WKInterfaceImage *chainImage;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *addressLabel;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *lbPrivateKey;

@end


@implementation WalletDetailController

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];
    
    self.info = context;
//    NSString *wallet = [NSString stringWithFormat:@"用户ID: %@\n钱包账户名称: %@\n钱包类型: %@\n钱包地址: %@\n钱包私钥: %@", self.info.account, self.info.name,self.info.strChain,self.info.addr, key];
    if ([_info.avatar isEqualToString:@""]) {
        [_headImage setImageNamed:@"headImg"];
    } else {
        [_headImage wkSetImageUrl:_info.avatar];
    }
    [_chainImage setImageNamed: _info.strChain];
    [_nameLabel setText: _info.name];
    [_addressLabel setText: _info.addr];
    if (_info.privKey.length > 16) {
        NSString *key = [self.info.privKey stringByReplacingCharactersInRange:NSMakeRange(6, self.info.privKey.length-10) withString:@"****"];
        [_lbPrivateKey setText: key];
    }

}

- (void)willActivate {
    // This method is called when watch view controller is about to be visible to user
    [super willActivate];
}

- (void)didDeactivate {
    // This method is called when watch view controller is no longer visible
    [super didDeactivate];
}

- (IBAction)deleteAction {
    WKAlertAction *actCancel = [WKAlertAction actionWithTitle:@"取消" style:WKAlertActionStyleCancel handler:^(void){
        return;
    }];
    WKAlertAction *actDel = [WKAlertAction actionWithTitle:@"删除" style:WKAlertActionStyleDefault handler:^(void){
        kCoreDataManager.isUpdate = true;
        [kCoreDataManager.persistentContainer.viewContext deleteObject:self.info];
        
        //保存--记住保存
        NSError *error = nil;
        if ([kCoreDataManager.persistentContainer.viewContext save:&error]) {
            NSLog(@"删除成功");
        }else{
            NSLog(@"删除数据失败, %@", error);
        }
        [self popController];
        
    }];
    
    NSArray *testing = @[actDel, actCancel];
    [self presentAlertControllerWithTitle:@"警告" message:@"确定删除后将永不能找回!" preferredStyle:WKAlertControllerStyleAlert actions:testing];
}

@end



