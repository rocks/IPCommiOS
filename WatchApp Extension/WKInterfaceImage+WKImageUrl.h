//
//  WKInterfaceImage+WKImageUrl.h
//  WatchApp Extension
//
//  Created by kiveen on 2019/5/24.
//  Copyright © 2019 wsl. All rights reserved.
//

#import <WatchKit/WatchKit.h>


@interface WKInterfaceImage (WKImageUrl)

- (void)wkSetImageUrl:(NSString *)imageUrl;

@end
