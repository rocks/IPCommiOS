//
//  WatchWCSessionManager.h
//  WatchApp Extension
//
//  Created by kiveen on 2019/5/23.
//  Copyright © 2019 wsl. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WatchConnectivity/WatchConnectivity.h>

//收到phone消息刷新界面的代理
@protocol WatchWCSessionManagerDelegate <NSObject>

/**收到消息回调*/
- (void)receiveMessage:(NSDictionary<NSString *,id> *)message replyHandler:(void (^)(NSDictionary<NSString *,id> *))replyHandler;

@end


@interface WatchWCSessionManager : NSObject

@property (nonatomic,strong) WCSession *session;
@property (nonatomic, assign) id<WatchWCSessionManagerDelegate> delegate;

/// 获取单例
+ (instancetype)sharedInstance;
/// 激活Session
- (void)startSession;

- (void)sendMsgToPhone:(NSDictionary *)msg;


@end


